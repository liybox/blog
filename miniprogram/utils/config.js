/**
 * 打赏二维码
 */
var moneyUrl ="http://file.xiaomutong.com.cn/IMG_0354-20200419-115328.JPG"

/**
 * 公众号二维码
 */
var wechatUrl ="http://file.xiaomutong.com.cn/IMG_0355-20200419-115621.JPG"

/**
 * 云开发环境
 */
var env ="avatar-zbqhx"
//var env ="test-91f3af"
/**
 * 个人文章操作枚举
 */
var postRelatedType = {
    COLLECTION: 1,
    ZAN: 2,
    properties: {
        1: {
            desc: "收藏"
        },
        2: {
            desc: "点赞"
        }
    }
};

var subcributeTemplateId="z1P6jLJxGep9eWBJ92HKsWzHLuTC-5XElETqVK7O35U"

module.exports = {
    postRelatedType: postRelatedType,
    moneyUrl:moneyUrl,
    wechatUrl:wechatUrl,
    env:env,
    subcributeTemplateId:subcributeTemplateId
}